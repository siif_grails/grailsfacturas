package factura

import grails.transaction.Transactional

import org.grails.cxf.utils.GrailsCxfEndpoint
import javax.jws.WebMethod
import javax.jws.WebResult
import javax.jws.WebParam

@Transactional
@GrailsCxfEndpoint
class FacturaWSService {

    @WebMethod
    @WebResult(name = "factura", targetNamespace = "") 
    def EncabezadoFactura guardarEncabezado(@WebParam(name = 'numFactura') String numFactura, 
                          @WebParam(name = 'fechaFactura') String fechaFactura, 
                          @WebParam(name = 'clienteId') String clienteId, 
                          @WebParam(name = 'nitCliente') String nitCliente, 
                          @WebParam(name = 'nombreCliente') String nombreCliente) {
        def datosFactura = [numFactura: numFactura, 
                            fecha:fechaFactura,
                            clienteId: clienteId,
                            nitCliente: nitCliente,
                            nombreCliente:nombreCliente
                            ]
println datosFactura                            
        def factura = new EncabezadoFactura();
        factura.numeroFactura = datosFactura.numFactura
        factura.fechaFactura =  Date.parse( 'yyyy-MM-dd', datosFactura.fecha)
        //factura.cliente = new Cliente(nombre: datosFactura.cliente, nit:"11111")
        //def cliente = getCliente(datosFactura.clienteId)
        def cliente
        if (datosFactura.clienteId != "-1") {
            cliente =Cliente.findById(datosFactura.clienteId);
        }
        else {
            cliente = new Cliente(nit:datosFactura.nitCliente, nombre: datosFactura.nombreCliente);
        }
        //def cliente = Cliente.findById(datosFactura.clienteId)
        factura.cliente = cliente

        factura.save(flush: true, failOnError: true);
        return(factura)
    }

    def listaFactura() {
    	def listadoFacturas = EncabezadoFactura.list(max:5, sort:"id", order:"desc")
    	return listadoFacturas
    }

    def listaClientes(){
        return Cliente.list()
    }

}