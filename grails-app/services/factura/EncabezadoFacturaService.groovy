package factura

import grails.transaction.Transactional

@Transactional
class EncabezadoFacturaService {

    def guardarFactura(datosFactura) {
println datosFactura
        def factura = new EncabezadoFactura();
        factura.numeroFactura = datosFactura.numFactura
        factura.fechaFactura =  Date.parse( 'yyyy-MM-dd', datosFactura.fecha)
        //factura.cliente = new Cliente(nombre: datosFactura.cliente, nit:"11111")
        //def cliente = getCliente(datosFactura.clienteId)
        def cliente
        if (datosFactura.clienteId != "-1") {
            cliente =Cliente.findById(datosFactura.clienteId);
        }
        else {
            cliente = new Cliente(nit:datosFactura.nitCliente, nombre: datosFactura.nombreCliente);
        }
        //def cliente = Cliente.findById(datosFactura.clienteId)
        factura.cliente = cliente

		/*Los ítems que componen el detalle de la factura en el form de html son 3 arrays de inputs con el mismo name: 
			Un array de inputs con el name=item,
			Un array de inputs con el name=cantidad
			Un array de inputs con el name=valor
		
		En el campo item viene el nombre del servicio a facturar, cantidad y valor (representan lo que su nombre indica)
		
		Pero si en la factura solo agregan un ítem, entonces el submit no envía estos campos como un array de valores, sino que los envía como un valor simple,
		como cualquier otro input.
		
		nuestro algoritmo para grabar los detalles está fundamentado en recorrer un array. Por tal razón si los campos del detalle no vienen como array sino como valor simple,
		la sentencia for nos va a dar un error.
		*/
		
		/*En el siguiente if, si el campo ítem de los parámetros que llegan del submit no es un array (esto se da cuando la factura solo tiene una fila de detalles), 
		entonces lo obligamos a que sea un array de al menos un elemento, y lo mismo debemos hacer
		por consiguiente con la cantidad y el valor. Los convertimos en un array de un solo valor. Los corchetes cuadrados en groovy se utilizan también para definir o
		inicializar un array
		*/
        if (!datosFactura.item.class.isArray()) {
            datosFactura.item =[datosFactura.item]
            datosFactura.cantidad =[datosFactura.cantidad]
            datosFactura.valor =[datosFactura.valor]
        }

		/* De aquí en adelante todo se realiza sabiendo que los ítems son un array de 1 o varios items */
        def cantidadItems = datosFactura.item.size()
        for (i in 0..cantidadItems-1) {
               factura.addToDetalles(new DetalleFactura(item: datosFactura.item[i], 
                                                cantidad: datosFactura.cantidad[i], 
                                                valor: datosFactura.valor[i])) 
        }
        
        factura.save(flush: true, failOnError: true);
    }

    def listaFactura(){
    	def listadoFacturas = EncabezadoFactura.list(max:5, sort:"id", order:"desc")
    	return listadoFacturas
    }

    def listaClientes(){
        return Cliente.list()
    }

    def guardarCliente(datosCliente){
        def cliente = new Cliente()
        cliente.nit = datosCliente.nit
        cliente.nombre = datosCliente.nombre
        cliente.save(flush: true, failOnError: true)
    }

    def getFactura(idFactura) {
        return EncabezadoFactura.get(idFactura)
    }
}