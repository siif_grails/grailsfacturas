package factura

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType

import grails.rest.*

@XmlAccessorType(XmlAccessType.NONE)
@Resource(uri='/facturas')
class EncabezadoFactura {

    @XmlElement
    String numeroFactura

    @XmlElement
    Date   fechaFactura

    Date   dateCreated

    Date   lastUpdated   

    Cliente cliente

    static transients = ['valorTotal']

    static hasMany = [detalles: DetalleFactura]

    @XmlElement
    Long getValorTotal() { detalles.sum { it.cantidad * it.valor} }

    String toString(){
        return "${numeroFactura + " " + cliente.nombre}"
    }   

    static constraints = {
        //cliente2 unique: true
        fechaFactura nullable:true;
    }

    static namedQueries = {
        facturaNumero{String num ->
            ilike ('numeroFactura', '%'+num)
        }

        clienteComo { String pCliente ->
            cliente {   
                    ilike ('nombre', '%'+pCliente+'%')
            } 
        }    
    }

  
}
