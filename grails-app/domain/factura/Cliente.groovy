package factura

class Cliente {

    String nombre
    String nit
    Date   dateCreated
    Date   lastUpdated   

    //static belongsTo = [factura: EncabezadoFactura]

    String toString(){
        return "${nombre}"
    }   

    static constraints = {
    }
  
}