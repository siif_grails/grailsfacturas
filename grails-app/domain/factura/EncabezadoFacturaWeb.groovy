package factura

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement

class EncabezadoFacturaWeb {

    @XmlElement
    String numeroFactura

    @XmlElement
    Date   fechaFactura

    Date   dateCreated
    Date   lastUpdated
    Cliente cliente

    @XmlElement
    String getNombreCliente() {
        return cliente.nombre
    }
    
    //static hasOne = [cliente2: Cliente]

    static hasMany = [detalles: DetalleFactura]

    String toString(){
        return "${numeroFactura + " " + cliente.nombre}"
    }   

    static constraints = {
        //cliente2 unique: true
    }

    static namedQueries = {
        clienteComo { String pCliente ->
println ("pCliente")            
println (pCliente)            
            cliente {
                or {
                    ilike ('nombre', '%'+pCliente+'%')
                    ilike ('nombre', '%'+'dro'+'%')                    
                }
            } 
        }    
    }

  
}