package factura

class DetalleFactura {

    String  item
    int     cantidad
    int     valor
    Date    dateCreated
    Date    lastUpdated 

    static belongsTo = [factura: EncabezadoFactura]  

    String toString(){
        return "${item}"
    }   

    static constraints = {
    }
  
}
