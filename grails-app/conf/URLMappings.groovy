class UrlMappings {
 static mappings = {
  "/$controller/$id?"{
    action = [GET:"show", POST:"save", PUT:"update", DELETE:"remove"]
  }
  "500"(view:'/error')
 }
}
