package factura
import grails.util.Environment

class BootStrap {

    def init = { servletContext ->
println "xxxxxxxxx";    	
        if(Environment.current == Environment.DEVELOPMENT) {

        	//factura 1
			def eFact = new EncabezadoFactura(	
								numeroFactura: "123451", fechaFactura: Date.parse( 'yyyy/MM/dd', '2017/05/01'), 
        						cliente: new Cliente(nombre: "Ferney Salazar", nit: "1234567-8") 
        					)
        					.addToDetalles( new DetalleFactura(item: "Servicio 1", cantidad: 10, valor: 75000) )
        					.addToDetalles( new DetalleFactura(item: "Servicio 2", cantidad: 12, valor: 87000) )
        					.save()        	
        	//factura 2 y 3
        	def cliente2 = new Cliente(nombre: "Pedro Páramox", nit:"9012345-6") 
			eFact = new EncabezadoFactura(	
								numeroFactura: "123452", fechaFactura: Date.parse( 'yyyy/MM/dd', '2017/05/08'), 
        						cliente: cliente2
        					)
        					.addToDetalles( new DetalleFactura(item: "Servicio 3", cantidad: 25, valor: 115000) )
        					.addToDetalles( new DetalleFactura(item: "Servicio 2", cantidad: 48,  valor: 87000) )
        					.save()        	
			eFact = new EncabezadoFactura(	
								numeroFactura: "123453", fechaFactura: Date.parse( 'yyyy/MM/dd', '2017/06/15'), 
        						cliente: cliente2
        					)
        					.addToDetalles( new DetalleFactura(item: "Servicio 4", cantidad: 48, valor: 96000) )
        					.save()
println "xxxx";        				
println "***todas las facturas****"	
println EncabezadoFactura.list();        					
println "***factura como %2****" 
println EncabezadoFactura.facturaNumero("2").list();  
println "***facturas clientes como fer****" 
println EncabezadoFactura.clienteComo("fer").list();        					
println "xxxx";
        }
    }
    def destroy = {
    }

}