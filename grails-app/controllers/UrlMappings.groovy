package factura

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')

    "/facturasM"{
        controller = "FacturaREST"
        action = [GET: "index"]
        }    

    "/facturasM/$id"{
        controller = "FacturaREST"
        action = [GET: "show"]
        }    

    }
}
