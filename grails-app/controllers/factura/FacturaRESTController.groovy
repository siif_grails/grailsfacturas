package factura

import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import static org.springframework.http.HttpMethod.*
import grails.converters.*

@Transactional(readOnly = true)
class FacturaRESTController {

def EncabezadoFacturaService

def index() {
	def listaFactura = EncabezadoFacturaService.listaFactura()	
	render listaFactura as JSON
}

def show() {
    def factura = EncabezadoFacturaService.getFactura(params.id)
    render factura as JSON
}

} //fin
