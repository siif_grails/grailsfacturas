package factura

class OpsFacturaController {

def EncabezadoFacturaService

def index() { 
    render "<h1>Especifique la página dentro del módulo de facturación</h1>"
}

def nuevaFactura() {
	def clientes = EncabezadoFacturaService.listaClientes()
    render(template: "nuevaFactura", model: [listaClientes: clientes, mensaje: "Encabezado Factura Grabado Correctamente"])
}

def verFactura() {
    render(view:"nuevaFactura",  model: [titulo: "Ver Factura"])
}

def guardarEncabezadoAjax() {
    EncabezadoFacturaService.guardarFactura(params)
    redirect(action:"listaFactura")
}

def listaFactura (){
	def listaFactura = EncabezadoFacturaService.listaFactura()	
    def  clientes = EncabezadoFacturaService.listaClientes() 
	return [lista:listaFactura, clientes:clientes]
}

def nuevoItem(){
    render(template: "nuevoItem", model: [mensaje: "Encabezado Factura Grabado Correctamente"])
}

def nuevoCliente(){
    render(template: "nuevoCliente")
}

def guardarCliente(){
	println params
	EncabezadoFacturaService.guardarCliente(params)
	render "<h4>Cliente guardado</h4>"
}
} //fin
