<html>

<head>
    <meta name="layout" content="main">
	<asset:javascript src="application.js"/>    
	<asset:stylesheet src="application.css"/>
</head>

<body >
<h1>FACTURAS<h1>
<table>
	<tr>
		<th>Id</th>
		<th>No Factura </th>
		<th>Nombre Cliente </th>
		<th>Fecha </th>
		<th>Valor Total</th>
	</tr>

	<g:render template="lista" collection="${lista}" />

</table>

<g:link action="nuevaFactura" onclick="nuevaFacturaAjax(); return false;">Nueva Factura</g:link>
<div id="nuevaFactura">
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>


<g:javascript>
function nuevaFacturaAjax() {
  $.ajax({
            url: "nuevaFactura",
            method:"post",
            dataType: 'html',
            success: function(data) {
            	$("#nuevaFactura").html(data);
                console.log(data); //<-----this logs the data in browser's console
            },
            error: function(xhr){
                alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
}

function nuevoItemAjax() {
  $.ajax({
            url: "nuevoItem",
            method:"post",
            dataType: 'html',
            success: function(data) {
            	$("#itemsFactura").append(data);
                console.log(data); //<-----this logs the data in browser's console
            },
            error: function(xhr){
                alert(xhr.responseText); //<----when no data alert the err msg
            }
        });
}

function callFormCliente(){
    $.ajax({
            type: "POST",
            url: "nuevoCliente",
            datatype: "html"
    }).success(function(data) {
        $('.modal-content').html(data);
        $('#myModal').modal('show');
    });
}

function saveCliente(pButton){
    var miForm = $(pButton).parents("form");
    var oData = new FormData(miForm);
    var url=miForm.attr("action");
    $.ajax({
         url:url,
         type:'GET',
         data:$(miForm).serialize(),
         success:function (req) {
                console.log("req "+req)
                $("#myModal").modal("hide");
               $("#ajaxMessage").html(req);
         },
         error:function (err){
            alert("Error: "+err);
         }
     });
}

</g:javascript>

<script>
var clientes = [
<g:each var="cliente" in="${clientes}">
  {id:"${cliente.id}", nit: "${cliente.nit}", nombre:"${cliente.nombre}"},
</g:each>
];

selectCliente = function(value) {
  if (value!="-1") {
    var _cliente = clientes.find(function(item) {
      return(item.id == value);
    });

    $("#nitCliente").val(_cliente.nit);
    $("#nombreCliente").val(_cliente.nombre);
  }
  else {
    $("#nitCliente").val("");
    $("#nombreCliente").val("");    
  }
}
</script>
</body>
</html>