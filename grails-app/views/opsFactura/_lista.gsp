<tr>
	<td>${it.id}</td>
	<td>${it.numeroFactura}</td>		
	<td>${it.cliente.nombre}</td>		
	<td><g:formatDate format="yyyy-MM-dd" date="${it.fechaFactura}"/></td>		
	<td><g:formatNumber number="${it.valorTotal}" format="###,###,##0" /></td>		
</tr>	