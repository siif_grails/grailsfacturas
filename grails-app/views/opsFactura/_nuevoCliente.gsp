<g:form name="myFormCliente" on404="alert('not found!')"
              url="[action:'guardarCliente']">
  
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Nuevo Cliente</h3>
  </div>

  <div class="modal-body">

      <div id="ajaxMessage">Datos básicos:</div>

      <div class="form-group">
        <label for="orderCons">NIT</label>
        <g:field type="number" name="nit" id="nit" required=""/>
      </div>
      <div class="form-group">
        <label for="status">Nombre(s)</label>
        <g:field type="text" name="nombre" id="nombre"/>
      </div>
  </div>
  <div class="modal-footer"> 
      <button type="button" class="btn btn-primary" onclick="saveCliente(this); return false;">Guardar</button>
      <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
  </div>

</g:form>
